<p align="center">
	A list of <strong>free resources</strong> to learn React Development
</p>

## Table Of Contents
- [Motivation](#motivation)
- [Essential Path](#essential-path)
	- [Why React?](#why-react)
	- [React](#react)
	- [ES2015+](#es2015)
	- [Routing](#routing)
	- [State Management](#state-management)
	- [Build Stuff](#build-stuff)
	- [Bundlers](#bundlers)
- [Optional Path](#optional-path)
	- [Typescript](#typescript)
- [Inspiration And Additional Resources](#inspiration-and-additional-resources)

## Motivation
This project aims to collect the **best free resources** for those that want to learn how to build applications with React and also understand the [concepts](https://github.com/reactjs/react-basic) that come with its adoption like Functional Programming, Composition, Unidirectional Data Flow and many others.

## Essential Path
### Why React?
1. [JS Apps at Facebook](https://www.youtube.com/watch?v=GW0rj4sNH2w)
1. [Why did we build React?](https://facebook.github.io/react/blog/2013/06/05/why-react.html)
1. [React: Rethinking best practices](https://www.youtube.com/watch?v=x7cQ3mrcKaY)

### [React](https://facebook.github.io/react/)
1. [React Docs - Quick Start section](https://facebook.github.io/react/docs/installation.html) `basic`
1. [React Docs - Tutorial: Intro to React](https://facebook.github.io/react/tutorial/tutorial.html) `basic`
1. [React Docs - Advanced Guides section](https://facebook.github.io/react/docs/jsx-in-depth.html) `advanced`


### ES2015+
1. [Learn ES6 (ECMAScript 2015)](https://egghead.io/courses/learn-es6-ecmascript-2015) `basic`
1. [Exploring ES6](http://exploringjs.com/es6/) `advanced`
1. [Exploring ES2016 and ES2017](https://leanpub.com/exploring-es2016-es2017/read) `advanced`

### Routing
1. [React Router Docs](https://react-router.now.sh/) `basic`

### State Management
#### [Redux](http://redux.js.org/)
1. [Getting Started with Redux](https://egghead.io/courses/getting-started-with-redux) `basic`
1. [Building React Applications with Idiomatic Redux](https://egghead.io/courses/building-react-applications-with-idiomatic-redux) `advanced`

### Build Stuff
1. [Several project ideas](https://react.rocks/)
1. [React - TodoMVC](http://todomvc.com/examples/react/#/)
1. [Bootstrapping a React project](https://auth0.com/blog/bootstrapping-a-react-project/)
1. [The SoundCloud Client in React + Redux](http://www.robinwieruch.de/the-soundcloud-client-in-react-redux/)
1. A Primer on the React Ecosystem: [1](http://patternhatch.com/2016/07/06/a-primer-on-the-react-ecosystem-part-1-of-3/), [2](http://patternhatch.com/2016/08/02/a-primer-on-the-react-ecosystem-part-2-of-3/) and 3.
1. [Building a React/Redux App with JSON Web Token (JWT) Authentication](http://blog.slatepeak.com/build-a-react-redux-app-with-json-web-token-jwt-authentication/)

### Bundlers
#### [Webpack](http://webpack.github.io/)
1. [Webpack your bags](https://blog.madewithlove.be/post/webpack-your-bags/) `basic`
1. [Webpack — The Confusing Parts](https://medium.com/@rajaraodv/webpack-the-confusing-parts-58712f8fcad9#.drs7xvnbi) `basic`
1. [SurviveJS - Webpack](http://survivejs.com/webpack/introduction/) `advanced`

## Optional Path
### Static Type Checkers
#### [Flow](https://flowtype.org/)

#### [Typescript](https://www.typescriptlang.org/)

1. [Gitbook Typescript deep dive](https://www.gitbook.com/book/basarat/typescript/details)
1. [React - Redux - Typescript - TodoMVC](https://github.com/jaysoo/todomvc-redux-react-typescript)
1. [React Typescript samples](https://github.com/Lemoncode/react-typescript-samples)


## Inspiration And Additional Resources

**Inspiration**

1. [You’re Missing the Point of React](https://medium.com/@dan_abramov/youre-missing-the-point-of-react-a20e34a51e1a#.qgt6xupid) 
1. [react-makes-you-sad](https://github.com/gaearon/react-makes-you-sad)
1. [react-howto](https://github.com/petehunt/react-howto)
1. [Your Timeline for Learning React](https://daveceddia.com/timeline-for-learning-react/)
1. [5 Steps for Learning React Application Development](http://developer.telerik.com/featured/5-steps-for-learning-react-application-development/)
1. [Path to Learning React](https://www.reddit.com/r/reactjs/comments/4r95aj/path_to_learning_react/)

**Additional Resources**

1. [React/Redux Links](https://github.com/markerikson/react-redux-links)
1. [Redux Ecosystem Links](https://github.com/markerikson/redux-ecosystem-links)
1. [Community Resources](https://github.com/markerikson/react-redux-links/blob/master/community-resources.md)